#!/usr/bin/env python
# ==========================
# author: dudley burrows <dudleyb@protonmail.com>
#   repo: https://gitlab.com/dudleyb/bd_func
# ==========================

import csv
import json
from pathlib import Path

log_files = Path("./").glob("*.log")

for log_file in log_files:

    device_id = ""
    json_file = "-thermistors.json"
    json_data = {}
    measurements = []

    csv_file = open(log_file, 'r')
    sensor_data = [line for line in csv_file if "Thermistors" in line]

    # get device_id from first row
    csv_data = csv.reader(sensor_data, delimiter=' ')
    for row in csv_data:
        device_id = row[2]
        break

    # use device_id for json output filename
    json_file = device_id + json_file

    # set deviceId in json data
    json_data['deviceId'] = device_id

    # get each row timestamp and thermistor outputs
    ## this needs to be done a better way, the temps
    ## may not always be in the same column
    for row in csv.reader(sensor_data, delimiter=' '):
        timestamp = row[0] + ' ' + row[1]
        message = {}
        message['timestamp'] = timestamp
        message['PRH'] = row[7]
        message['PLH'] = row[11]
        message['PRT'] = row[15]
        message['PLT'] = row[19]
        measurements.append(message)

    # include all data into json output
    json_data['measurements'] = measurements

    # dump json output to file
    with open(json_file, 'w') as f:
        json.dump(json_data, f, indent=4)
